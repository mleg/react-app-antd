const changeEntryPoint = require('./config/change-entry-point');
const configureLessLoaders = require('./config/configure-less-loaders');

module.exports = {
  chainWebpack: config => {
    changeEntryPoint(config);
    configureLessLoaders(config);
  }
};
