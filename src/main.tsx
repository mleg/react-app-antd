import { App } from '@/App';
import '@/styles/load-antd-styles';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(<App />, document.getElementById('app'));
