import React from 'react';
import { Button, Input } from 'antd';
const css = require('./App.less');

export class App extends React.Component {
  render() {
    return (
      <div>
        This React App works
        <Button type="primary">Button</Button>
        <div className={css.simpleDelimiter}>-----</div>
      </div>
    );
  }
}
