const pathBuilder = require('path');
const variables = require('../src/styles/variables');
const rootDir = require('../root-dir');

function setLessLoaderOptions(options) {
  return {
    ...options,
    javascriptEnabled: true,
    modifyVars: variables
  };
}

module.exports = config => {
  config.module
    .rule('less')
    .oneOf('normal')
    .use('less-loader')
    .tap(options => {
      return setLessLoaderOptions(options);
    });
  config.module
    .rule('less')
    .oneOf('normal-modules')
    .set('test', /\.less$/)
    .merge({
      include: [pathBuilder.resolve(rootDir, 'src')],
      exclude: [pathBuilder.resolve(rootDir, 'src', 'styles')]
    })
    .use('less-loader')
    .tap(options => {
      return setLessLoaderOptions(options);
    });
};
