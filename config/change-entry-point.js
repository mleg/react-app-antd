module.exports = config => {
  config
    .entry('app')
    .clear()
    .add('./src/main.tsx');
};
